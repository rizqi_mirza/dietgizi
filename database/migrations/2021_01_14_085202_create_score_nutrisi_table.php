<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoreNutrisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('score_nutrisi', function (Blueprint $table) {
            $table->id('id_score',11);
            $table->bigInteger('id_scoreDataHitung')->unsigned();
            $table->foreign('id_scoreDataHitung')->references('id_dataHitung')->on('data_hitung');
            $table->string('scoreBMR');
            $table->string('scoreEnergi');
            $table->string('scoreProtein');
            $table->string('scoreLemak');
            $table->string('scoreKarbohidrat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('score_nutrisi');
    }
}
