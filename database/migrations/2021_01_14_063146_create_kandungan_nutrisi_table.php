<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKandunganNutrisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kandungan_nutrisi', function (Blueprint $table) {
            $table->bigIncrements('id_kandungan',11);
            $table->bigInteger('kategoriKandunganID')->unsigned();
            $table->foreign('kategoriKandunganID')->references('id_kategori')->on('kategori_panduan');
            $table->string('namaMakananMinuman');
            $table->string('urt');
            $table->integer('berat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kandungan_nutrisi');
    }
}
