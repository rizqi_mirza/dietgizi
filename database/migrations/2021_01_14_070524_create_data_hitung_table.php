<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataHitungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_hitung', function (Blueprint $table) {
            $table->id('id_dataHitung',11);
            $table->bigInteger('id_userDataHitung')->unsigned();
            $table->foreign('id_userDataHitung')->references('id_user')->on('users');
            $table->string('umur');
            $table->string('berat_badan');
            $table->string('tinggi_badan');
            $table->string('aktivitas');
            $table->string('faktor_stres');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_hitung');
    }
}
