<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/login', function () {
    return view('login');
});
Route::get('/logout','UserController@logout');

Route::post('/auth-login','UserController@authenticated');

Route::get('/register', function () {
    return view('register');
});
Route::post('/post-register','UserController@create');

Route::get('/dashboard', function(){
    return view('admin.dashboard-admin');
});


Route::get('/index-gizi',function () {
    return view('form-hitung-pasien');
});
Route::post('/create-data','ScoreDietGiziController@create');


//data nutrisi
Route::get('/data-nutrisi','PanduanNutrisiController@indexPanduan');


//hitung-nutrisi
Route::get('/index-hitung-nutrisi','HitungNutrisiController@indexHitung');
Route::post('/hitung-nutrisi', 'HitungNutrisiController@create');

Route::get('/hasil-nutrisi/{id}','HitungNutrisiController@hasil');
Route::post('/create-score','HitungNutrisiController@createScore');

Route::get('/data-nutrisi','HitungNutrisiController@dataHasilNutrisi');
