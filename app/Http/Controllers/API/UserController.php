<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function create(Request $request){
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            "Success" => 1,
            "Message" => "Selamat Datang",
            "User" => $user
        ]);
    }

    public function authenticated(Request $request){
        $email = $request->email;
        $password = $request->password;

        $user = User::where('email',$email)->first();
        if($user){
            if(Hash::check($password, $user->password)){
                Auth::login($user, true);
                return response()->json([
                    "Success" => 1,
                    "Message" => "Selamat Datang",
                    "User" => $user
                ]);
            }else{
                return $this->error('Password Salah');
            }
        }else{
            return $this->error('Password Salah');
        }

    }


}
