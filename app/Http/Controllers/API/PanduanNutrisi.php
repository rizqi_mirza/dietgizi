<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Model\KategoriPanduan;
use App\Model\KandunganNutrisi;


class PanduanNutrisi extends Controller
{
    public function indexKategori(){

        $kategori  = KategoriPanduan::all();
        return response()->json(
            [$kategori,200]
        );

    }
    public function createKategori(Request $request){

        $kategori = new KategoriPanduan;
        $kategori->nama_kategori = $request->nama_kategori;
        $kategori->save();

       return response()->json([
           "Success" => 1,
           "Message" => "Kategori Berhasil disimpan",
           "Data" => $kategori
       ]);
    }
    public function createKandunganNutrisi(Request $request){
        $panduan = new KandunganNutrisi;
        $panduan->kategoriKandunganID = $request->id_kategori;
        $panduan->namaMakananMinuman = $request->nama_nutrisi;
        $panduan->urt = $request->satuan;
        $panduan->berat = $request->berat;
        $panduan->save();

        return response()->json([
            "Success" => 1,
            "Message" => "Data Panduan berhasil di input",
            "Data" => $panduan
        ]);
    }

    public function indexPanduan(){
        $panduan = KandunganNutrisi::join('kategori_panduan','kandungan_nutrisi.kategoriKandunganID','=','kategori_panduan.id_kategori')->get();

        return response()->json()([
            $panduan,200
        ]);
    }
}
