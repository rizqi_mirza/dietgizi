<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\HitungNutrisi;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Model\ScoreNutrisi;

class HitungNutrisiController extends Controller
{
    public function dataHasilNutrisi(){
        $userID = Auth::User()->id_user;
        $data = ScoreNutrisi::join('data_hitung','score_nutrisi.id_scoreDataHitung','=','data_hitung.id_dataHitung')
        ->join('users','data_hitung.id_userDataHitung','=','users.id_user')->where('id_user',$userID)->get();

        // return view('admin.data-hasil-nutrisi',compact('data'));
        return response()->json([
            $data , 20
        ]);

    }
}
