<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\HitungNutrisi;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Model\ScoreNutrisi;

class HitungNutrisiController extends Controller
{
    public function indexHitung(){
        $hitung = HitungNutrisi::join('users','data_hitung.id_userDataHitung','=','users.id_user')->get()->first();

        return view('admin.index-hitung', compact('hitung'));
    }
    public function create(Request $request){
        $hitung = new HitungNutrisi;
        $hitung->id_userDataHitung = Auth::user()->id_user;
        $hitung->umur = $request->umur_pasien;
        $hitung->berat_badan = $request->berat_badan;
        $hitung->tinggi_badan = $request->tinggi_badan;
        $hitung->aktivitas = $request->aktivitas;
        $hitung->faktor_stres = $request->stres;
        $hitung->save();

        return redirect('/hasil-nutrisi/'.$hitung->id_dataHitung);
    }

    public function hasil(Request $request,$id){
        $formHitung = HitungNutrisi::join('users','data_hitung.id_userDataHitung','=','users.id_user')
        ->where('id_dataHitung',$id)->get()->first();
        // dd($formHitung);
        // $beratBadanIdeal =$this->BeratBadanIdeal($formHitung->berat_badan,$formHitung->tinggi_badan);
        $hitungBMR = $this->HitungBMR($formHitung->jenis_kelamin, $formHitung->berat_badan , $formHitung->tinggi_badan);
        $hasilBMR = $this->HasilBMR($formHitung->jenis_kelamin, $hitungBMR['hitungBMR'], $formHitung->umur);
        $energi = $this->energi($hasilBMR['scoreBMR'],$formHitung->aktivitas,$formHitung->faktor_stres);
        $protein = $this->protein($formHitung->berat_badan);
        $lemak = $this->lemak($energi['scoreEnergi']);
        $karbohidrat = $this->karbohidrat($energi['scoreEnergi'],$protein['protein'],$lemak['lemak']);

        // dd($formHitung);
        return view('admin.hasil-nutrisi',compact('formHitung','hasilBMR','energi','protein','lemak','karbohidrat'));
    }

    // protected function BeratBadanIdeal($bb,$tb){
    //     $Imt = $bb/($tb*$tb);
    //     $hasilBBI = ($tb-100)-(10%($tb-100));

    //     return compact('Imt','hasilBBI');
    // }

    protected function HitungBMR($jk,$bb,$tb){
        $jk = strtoupper($jk);

        if($jk == "LAKI-LAKI"){
            $hitungBMR = 66+(13.7*$bb)+(5*$tb);
        }else{
            $hitungBMR = 655+(9.6*$bb)+(1.8*$tb);
        }

        return compact('hitungBMR');
    }

    protected function HasilBMR($jk,$hitungBMR,$umur){
        $jk = strtoupper($jk);
        if($jk == "LAKI-LAKI"){
            $scoreBMR = ((float)$hitungBMR)-(6.78*$umur);
        }else{
            $scoreBMR = ((float)$hitungBMR)-(4.7*$umur);
        }

        return compact('scoreBMR');
    }
    protected function energi($scoreBMR,$fa,$fs){
        $scoreEnergi = ((float)$scoreBMR)*$fa*$fs;

        return compact('scoreEnergi');
    }

    protected function protein($bb){
        $protein = 0.8*$bb;
        $scoreProtein = $protein;

        return compact('protein','scoreProtein');
    }

    protected function lemak($scoreEnergi){
        $lemak =  0.25*$scoreEnergi;
        $scoreLemak = $lemak/9;

        return compact('lemak','scoreLemak');
    }

    protected function karbohidrat($scoreEnergi,$protein,$lemak){
        $scoreKarbohidrat = ($scoreEnergi-$protein-$lemak)/4;

        return compact('scoreKarbohidrat');
    }

    public function createScore(Request $request){
        $this->validate($request , [
            "score_bmr" => 'required',
            "score_energi" => 'required',
            "score_protein" => 'required',
            "score_lemak" => 'required',
            "score_karbohidrat" => 'required'
        ]);

        $score = new ScoreNutrisi;
        $score->id_scoreDataHitung = $request->id_datahitung;
        $score->scoreBMR = $request->score_bmr;
        $score->scoreEnergi = $request->score_energi;
        $score->scoreProtein = $request->score_protein;
        $score->scoreLemak = $request->score_lemak;
        $score->scoreKarbohidrat = $request->score_karbohidrat;
        $score->save();
        // dd($score);
        return redirect('/data-nutrisi');
    }

    public function dataHasilNutrisi(){
        $userID = Auth::User()->id_user;
        $data = ScoreNutrisi::join('data_hitung','score_nutrisi.id_scoreDataHitung','=','data_hitung.id_dataHitung')
        ->join('users','data_hitung.id_userDataHitung','=','users.id_user')->where('id_user',$userID)->get();

        return view('admin.data-hasil-nutrisi',compact('data'));

    }
}
