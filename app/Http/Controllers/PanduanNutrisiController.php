<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\KategoriPanduan;
use App\Model\KandunganNutrisi;

class PanduanNutrisiController extends Controller
{
    public function indexKategori(){

        $kategori  = KategoriPanduan::all();
        return view('admin.data-kategori', compact('kategori'));

    }

    public function createKategori(Request $request){

        $kategori = new KategoriPanduan;
        $kategori->nama_kategori = $request->nama_kategori;
        $kategori->save();

        return redirect('/data-kategori');
    }

    public function createKandunganNutrisi(Request $request){
        $panduan = new KandunganNutrisi;
        $panduan->kategoriKandunganID = $request->id_kategori;
        $panduan->namaMakananMinuman = $request->nama_nutrisi;
        $panduan->urt = $request->satuan;
        $panduan->berat = $request->berat;
        $panduan->save();

    }

    public function indexPanduan(){
        // $panduan = KandunganNutrisi::join('kategori_panduan','kandungan_nutrisi.kategoriKandunganID ','=','kategori_panduan.id_kategori')->get();
        // return view('admin.data-nutrisi', compact('panduan'));
        $panduan = KandunganNutrisi::join('kategori_panduan','kandungan_nutrisi.kategoriKandunganID','=','kategori_panduan.id_kategori')->get();
        // dd($panduan);
        return view('admin.data-nutrisi', compact('panduan'));
    }




}
