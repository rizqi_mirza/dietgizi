<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function create(Request $request){
        $user = new User;
        $user->name = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect('/login');
    }

    public function authenticated(Request $request){
        $email = $request->email;
        $password = $request->password;

        $user = User::where('email',$email)->first();
        if($user){
            if(Hash::check($password, $user->password)){
                Auth::login($user, true);
                return redirect('/dashboard');
            }else{
                return redirect('/login')->with('alert','password atau email salah');
            }
        }else{
            return redirect('/login')->with('alert','password atau email salah');
        }

    }

    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect('login')->with('alert','Kamu sudah logout');
    }



}
