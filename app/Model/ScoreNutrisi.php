<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ScoreNutrisi extends Model
{
    protected $table = "score_nutrisi";
    protected $primaryKey = 'id_score';
    protected $fillable = ['scoreBMR','scoreEnergi','scoreProtein','scoreLemak','scoreKarbohidrat'];
}
