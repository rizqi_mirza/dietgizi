<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HitungNutrisi extends Model
{
    protected $table = "data_hitung";
    protected $primaryKey = 'id_dataHitung';
    protected $fillable = ['umur','berat_badan','tinggi_badan','aktivitas','faktor_stres'];

}
