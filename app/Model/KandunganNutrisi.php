<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KandunganNutrisi extends Model
{
    protected $table = "kandungan_nutrisi";
    protected $primaryKey = 'id_kandungan';
    protected $fillable = ['namaMakananMinuman','urt','berat'];
}
