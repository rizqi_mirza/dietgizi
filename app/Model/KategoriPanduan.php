<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KategoriPanduan extends Model
{
    protected $table = "kategori_panduan";
    protected $primaryKey = 'id_kategori';
    protected $fillable = ['nama_kategori'];
}
