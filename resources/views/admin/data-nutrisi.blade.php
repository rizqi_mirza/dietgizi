@extends('admin/master')

@section('main')


        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">DataTables</h1>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="./">Home</a></li>
              <li class="breadcrumb-item">Tables</li>
              <li class="breadcrumb-item active" aria-current="page">DataTables</li>
            </ol>
          </div>

          <!-- Row -->
          <div class="row">
            <!-- Datatables -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">DataTables</h6>
                  <a href="{{url('tambah-data-nutrisi')}}" class="btn btn-info mb-1">Tambah Daftar Makanan & Minuman</a>
                </div>
                <div class="table-responsive p-3">
                @if (!empty($panduan))
                  <table class="table align-items-center table-flush" id="dataTable">
                    <thead class="thead-light">
                      <tr>
                        <th>No</th>
                        <th>Nama Makanan & Minuman</th>
                        <th>Kategori Makanan</th>
                        <th>Satuan</th>
                        <th>Jml. Kalori</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Nama Makanan & Minuman</th>
                        <th>Kategori Makanan</th>
                        <th>Satuan</th>
                        <th>Jml. Kalori</th>
                        <th>Aksi</th>
                      </tr>
                    </tfoot>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @foreach($panduan as $value)
                      <tr>
                        <td>{{$no++}}</td>
                        <td>{{$value->namaMakananMinuman}}</td>
                        <td>{{$value->nama_kategori}}</td>
                        <td>{{$value->urt}}</td>
                        <td>{{$value->berat}}</td>
                        <td><a href="{{url('/edit-data-nutrisi/'.$value->id_panduanNutrisi)}}" class="btn btn-success btn-sm"><i class="fas fa-check"></i></a>
                            <a href="{{url('/delete-data-nutrisi/'.$value->id_panduanNutrisi)}}" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                            {{--  <a href="#" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></a></td>  --}}
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                @else
                    <p>Data Kosong</p>
                @endif

                </div>
              </div>
            </div>

          <!--Row-->


          <!-- Modal Logout -->
          <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Are you sure you want to logout?</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                  <a href="{{url('/logout')}}" class="btn btn-primary">Logout</a>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!---Container Fluid-->




  <!-- Page level custom scripts -->
  <script>
    $(document).ready(function () {
      $('#dataTable').DataTable(); // ID From dataTable
      $('#dataTableHover').DataTable(); // ID From dataTable with Hover
    });
  </script>

@stop
