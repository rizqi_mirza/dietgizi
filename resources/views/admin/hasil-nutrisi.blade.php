@extends('admin/master')

@section('main')
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Tambah Pasien</h1>
      {{-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Forms</li>
        <li class="breadcrumb-item active" aria-current="page">Form Basics</li>
      </ol> --}}
    </div>

    <div class="row">
      <div class="col-lg-12">
        <!-- Form Basic -->
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Pasien Diet Gizi</h6>
          </div>
          <div class="card-body">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Pasien</label>
                    <input type="text" class="form-control" name="nama_pasien" value="{{Auth::user()->name}}" readonly>
                </div>


              {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
            </form>
          </div>
        </div>

        <div class="card mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Form Hitung Diet Gizi</h6>
            </div>
            <div class="card-body">
              <form action="" method="POST">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="gender" value="{{$formHitung->jenis_kelamin}}" readonly>
                      <option value="{{$formHitung->jenis_kelamin}}">{{$formHitung->jenis_kelamin}}</option>

                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Umur</label>
                    <input type="text" class="form-control" name="umur_pasien" value="{{$formHitung->umur}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Berat Badan</label>
                    <input type="text" class="form-control" name="berat_badan" value="{{$formHitung->berat_badan}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tinggi Badan</label>
                    <input type="text" class="form-control" name="tinggi_badan" value="{{$formHitung->tinggi_badan}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Aktivitas Fisik</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="aktivitas" value="{{$formHitung->aktivitas_fisik}}" readonly>
                      {{-- <option value="-">-pilih-</option> --}}
                      <option value="1.2">Sedenter (minim aktivitas fisik,jarang/tak pernah olahraga)</option>
                      <option value="1.375">Sedikit Aktif (Olahraga Ringan 1-3 hari seminggu)</option>
                      <option value="1.55">Normal / Cukup (Olahraga sedang, 3-5 hari seminggu)</option>
                      <option value="1.725">Aktif (Olahraga berat 6-7 hari seminggu)</option>
                      <option value="1.9">Sangat Aktif(olahraga 2x sehari atau pekerjaan sehari-hari menuntut fisik)</option>
                    </select>
                </div>
                {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
              </form>
            </div>
          </div>
          <div class="card mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Hasil Hitung Diet Gizi</h6>
            </div>
            <div class="card-body">
              <form action="/create-score" method="POST">
                @csrf
                <input type="hidden" class="form-control" name="id_datahitung"  value="{{$formHitung->id_dataHitung}}">
                <div class="form-group">
                    <label for="exampleInputEmail1">Hasil BMR</label>
                    <input type="text" class="form-control" name="score_bmr" value="{{round($hasilBMR['scoreBMR'],2)}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Total Energi</label>
                    <input type="text" class="form-control" name="score_energi" value="{{round($energi['scoreEnergi'],2)}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Protein</label>
                    <input type="text" class="form-control" name="score_protein" value="{{round($protein['scoreProtein'],2)}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Lemak</label>
                    <input type="text" class="form-control" name="score_lemak" value="{{round($lemak['scoreLemak'],2)}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Karbohidrat</label>
                    <input type="text" class="form-control" name="score_karbohidrat" value="{{round($karbohidrat['scoreKarbohidrat'],2)}}" readonly>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>

      </div>


    <!--Row-->



    <!-- Modal Logout -->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to logout?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            <a href="{{url('/logout')}}" class="btn btn-primary">Logout</a>
          </div>
        </div>
      </div>
    </div>

  </div>
@stop
