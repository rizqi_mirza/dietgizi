@extends('admin/master')

@section('main')
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Tambah Pasien</h1>
      {{-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Forms</li>
        <li class="breadcrumb-item active" aria-current="page">Form Basics</li>
      </ol> --}}
    </div>

    <div class="row">
      <div class="col-lg-12">
        <!-- Form Basic -->
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Pasien Diet Gizi</h6>
          </div>
          <div class="card-body">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Pasien</label>
                    <input type="text" class="form-control" name="nama_pasien" value="{{Auth::user()->name}}"  readonly>
                </div>
              {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
            </form>
          </div>
        </div>

        <div class="card mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Form Hitung Diet Gizi</h6>
            </div>
            <div class="card-body">
              <form action="{{url('/hitung-nutrisi')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="gender">
                        <option value="-"><span class="ai-amazon"></span></option>
                        <option value="Laki-Laki">Laki-Laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Umur</label>
                    <input type="text" class="form-control" name="umur_pasien" >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Berat Badan</label>
                    <input type="text" class="form-control" name="berat_badan" >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tinggi Badan</label>
                    <input type="text" class="form-control" name="tinggi_badan" >
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Aktivitas Fisik</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="aktivitas">
                      <option value="-">-pilih-</option>
                      <option value="1.2">Sedenter (minim aktivitas fisik,jarang/tak pernah olahraga)</option>
                      <option value="1.375">Sedikit Aktif (Olahraga Ringan 1-3 hari seminggu)</option>
                      <option value="1.55">Normal / Cukup (Olahraga sedang, 3-5 hari seminggu)</option>
                      <option value="1.725">Aktif (Olahraga berat 6-7 hari seminggu)</option>
                      <option value="1.9">Sangat Aktif(olahraga 2x sehari atau pekerjaan sehari-hari menuntut fisik)</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Faktor Stres</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="stres">
                      <option value="-">-pilih-</option>
                      <option value="1.1">Tidak Ada Stres, Status Gizi Normal</option>
                      <option value="1.3">Stres Ringan (Peradangan Saluran Cerna, Kanker, Bedah Efektif, Trauma, Demam, Operasi, Cidera Kepala Ringan)</option>
                      <option value="1.5">Stres Sedang (Sepsis, Bedah Tulang, Luka Bakar, Penyakit Hati)</option>
                      <option value="1.6">Stres Berat (HIV Aids+Komplikasi, Bedah Multisistem, TB Paru + Komplikasi)</option>
                      <option value="1.7">Stres Sangat Berat (Luka Kepala Berat)</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>

      </div>


    <!--Row-->



    <!-- Modal Logout -->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to logout?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            <a href="{{url('/logout')}}" class="btn btn-primary">Logout</a>
          </div>
        </div>
      </div>
    </div>

  </div>
@stop
