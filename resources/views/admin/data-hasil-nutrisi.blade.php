@extends('admin/master')

@section('main')


        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">DataTables</h1>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="./">Home</a></li>
              <li class="breadcrumb-item">Tables</li>
              <li class="breadcrumb-item active" aria-current="page">DataTables</li>
            </ol>
          </div>

          <!-- Row -->
          <div class="row">
            <!-- Datatables -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">DataTables</h6>
                </div>
                <div class="table-responsive p-3">
                @if (!empty($data))
                  <table class="table align-items-center table-flush" id="dataTable">
                    <thead class="thead-light">
                      <tr>
                        <th>No</th>
                        <th>Nama Pasien</th>
                        <th>Score BMR</th>
                        <th>Energi</th>
                        <th>Protein</th>
                        <th>Lemak</th>
                        <th>Karbohidrat</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Nama Pasien</th>
                        <th>Score BMR</th>
                        <th>Energi</th>
                        <th>Protein</th>
                        <th>Lemak</th>
                        <th>Karbohidrat</th>
                      </tr>
                    </tfoot>
                    <tbody>
                        @php
                        $no = 1;
                    @endphp
                    @foreach($data as $value)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->scoreBMR}}</td>
                            <td>{{$value->scoreEnergi}}</td>
                            <td>{{$value->scoreProtein}}</td>
                            <td>{{$value->scoreLemak}}</td>
                            <td>{{$value->scoreKarbohidrat}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                  </table>
                @else
                  <p>Data Kosong</p>
                @endif

                </div>
              </div>
            </div>

          <!--Row-->


          <!-- Modal Logout -->
          <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Are you sure you want to logout?</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                  <a href="{{url('/logout')}}" class="btn btn-primary">Logout</a>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!---Container Fluid-->




  <!-- Page level custom scripts -->
  <script>
    $(document).ready(function () {
      $('#dataTable').DataTable(); // ID From dataTable
      $('#dataTableHover').DataTable(); // ID From dataTable with Hover
    });
  </script>

@stop
